# OP32
OP32 is a 16-bit processor emulator with 32 op codes written in C.

The processor is comprized of

 - 19 16-bit registers
 - 64 Word Stack
 - 32x16 Pixel Screen Size
 - 2^16 Word memory size

The 32 Instructions are:

 - ```NOP``` -- Do nothing
 - ```LD [REG] [ADD]```    -- Load to REGSITER from ADDRESS
 - ```LDR [REG] [REG]```  -- Load to REGISTER from MEMORY[REGISTER]
 - ```LDV [REG] [VAL]```   -- Set REGISTER to VALUE
 - ```STO [ADD] [REG]```  -- Store to ADDRESS from REGISTER
 - ```STOR [REG] [REG]```  -- Store to MEMORY[REGISTER] from REGISTER
 - ```ADDV [REG] [VAL]```  -- Add on REGISTER VALUE
 - ```SUBV [REG] [VAL]```  -- Subtract on REGISTER VALUE
 - ```ADDR [REG] [REG]```  -- Add on REGISTER REGSITER
 - ```SUBR [REG] [REG]```  -- Subtract on REGISTER REGISTER
 - ```MULV [REG] [VAL]```  -- Multiply on REGISTER by VALUE
 - ```DIVV [REG] [VAL]```  -- Divide on REGISTER by VALUE
 - ```MULR [REG] [REG]```  -- Multiply on REGISTER by REGISTER
 - ```DIVR [REG] [REG]```  -- Divide on REGISTER by REGISTER
 - ```PUSH [REG]```  -- Push REGISTER to stack
 - ```POP [REG]```  -- Pop to REGISTER from stack
 - ```JMP [REG]```  -- Jump to REGISTER
 - ```JMPZR [REG] [REG]```  -- Jump if REGISTER == 0 to REGISTER
 - ```JMPNZR [REG] [REG]```  -- Jump if REGISTER != 0 to REGISTER
 - ```JMPNZ [REG] [REG]```  -- Jump if REGISTER == 0 to VALUE
 - ```JMPNZV [REG] [REG]```  -- Jump if REGISTER != 0 to VALUE
 - ```AND [REG] [REG]```  -- AND REGISTER with REGISTER Store on first
 - ```OR [REG] [REG]```  -- OR REGISTER with REGISTER Store on first
 - ```XOR [REG] [REG]```  -- XOR REGISTER with REGISTER Store on first
 - ```NAND [REG] [REG]```  -- NAND REGISTER with REGISTER Store on first
 - ```NOR [REG] [REG]```  -- NOR REGISTER with REGISTER Store on first
 - ```NOT [REG]```  -- Negate
 - ```LSH [REG] [REG]```  -- Shift REGISTER left by REGISTER
 - ```RSH [REG] [REG]```  -- Shift REGISTER right by REGISTER
 - ```CPY [REG] [REG]```  -- Copy to REGISTER from REGISTER
 - ```SWP [REG] [REG]```  -- Swap REGISTER with REGISTER
 - ```MOD [REG] [REG]```  -- Modue REGISTER by REGISTER Store on first