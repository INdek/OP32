CC=gcc
LNK=-lmingw32 -lSDLmain -lSDL
SDL_S=-mwindows
CFLAGS=-O0 -Wall -Wextra -g3

OP32=src/op32.c
OPCODES=src/opcodes.c
MAIN=src/main.c
TEST=src/test.c
VIDEO=src/video.c
AS=as/as.c

OUT_AS=op32-as
OUT_TEST=op32-test
OUT=op32


run:
	$(CC) $(MAIN) $(OP32) $(OPCODES) $(VIDEO) $(CFLAGS) $(SDL_S) $(LNK) -o $(OUT)

test:
	$(CC) $(TEST) $(OP32) $(OPCODES) $(VIDEO) $(CFLAGS) $(SDL_S) $(LNK) -o $(OUT_TEST)

.PHONY: as	
as:
	$(CC) $(AS) $(CFLAGS) -o $(OUT_AS)

clean:
	del *.o $(OUT) $(OUT_TEST) $(OUT_AS)