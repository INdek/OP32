#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include "as.h"

#define OP32_REG_NUM 19
#define OP32_MEM_SZ 0xFFFF

#define ERROR(...) do{								 \
				   	   printf("[ERROR]: "__VA_ARGS__); \
				   	   as_exit(1);				     \
				   }while(0)

#define WARNING(...) do{				   				   \
					 	 printf("[WARNING]: "__VA_ARGS__);   \
					 }while(0)


instruction_t inst_table[] = {
		{"nop"   , 3, 0x00, 0, {NOARG, NOARG}},
		{"ld"    , 2, 0x01, 2, {REG, VAL}},
		{"ldr"   , 3, 0x02, 2, {REG, REG}},
		{"ldv"   , 3, 0x03, 2, {REG, VAL}},
		{"sto"   , 3, 0x04, 2, {VAL, REG}},
		{"stor"  , 4, 0x05, 2, {REG, REG}},
		{"addv"  , 4, 0x06, 2, {REG, VAL}},
		{"subv"  , 4, 0x07, 2, {REG, VAL}},
		{"addr"  , 4, 0x08, 2, {REG, REG}},
		{"subr"  , 4, 0x09, 2, {REG, REG}},
		{"mulv"  , 4, 0x0A, 2, {REG, VAL}},
		{"divv"  , 4, 0x0B, 2, {REG, VAL}},
		{"mulr"  , 4, 0x0C, 2, {REG, REG}},
		{"divr"  , 4, 0x0D, 2, {REG, REG}},
		{"push"  , 4, 0x0E, 1, {REG, NOARG}},
		{"pop"   , 3, 0x0F, 1, {REG, NOARG}},
		{"jmp"   , 3, 0x10, 1, {REG, NOARG}},
		{"jmpzr" , 5, 0x11, 2, {REG, REG}},
		{"jmpnzr", 6, 0x12, 2, {REG, REG}},
		{"jmpzv" , 5, 0x13, 2, {REG, VAL}},
		{"jmpnzv", 6, 0x14, 2, {REG, VAL}},
		{"and"   , 3, 0x15, 2, {REG, REG}},
		{"or"    , 2, 0x16, 2, {REG, REG}},
		{"xor"   , 3, 0x17, 2, {REG, REG}},
		{"nand"  , 4, 0x18, 2, {REG, REG}},
		{"nor"   , 3, 0x19, 2, {REG, REG}},
		{"not"   , 3, 0x1A, 1, {REG, NOARG}},
		{"lsh"   , 3, 0x1B, 2, {REG, REG}},
		{"rsh"   , 3, 0x1C, 2, {REG, REG}},
		{"cpy"   , 3, 0x1D, 2, {REG, REG}},
		{"swp"   , 3, 0x1E, 2, {REG, REG}},
		{"mod"   , 3, 0x1F, 2, {REG, REG}}
};

label_t label_table[1024] = {};
uint16_t label_table_sz = 0;

FILE *fp;
FILE *out;


uint8_t check_label(char *p, uint16_t l){
	while(*p == ' ' || *p == '\t') p++;	//discard whitespace

	char buf[1024];
	char *m = &buf[0];
	int i=0,z;
	while(*p != ':' && *p != '\n'){
		*m = *p;
		i++;
		m++;
		p++;
	}

	*m = '\0';
	m -= i;

	if(*p==':'){
		for(z=0;z<label_table_sz;z++) //check if the label already exists
			if(strcmp(m,label_table[z].name) == 0) return 0;

		label_table[label_table_sz].name = malloc(i);
		strcpy(label_table[label_table_sz].name, m);
		label_table[label_table_sz].addr=l+1;
		get_line_addr((int *) &label_table[label_table_sz].addr);
		label_table_sz++;
	}

	return *p==':';
}

uint16_t decode_op(char *p){
	uint16_t code,code_m,code_s;

	/*Instructions with smaller names that came up first
	 * such as LD were matching with other instructions
	 * with similar names such as LDR or LDV*/

	code_m = 0; //the number of the code at which the largest match is found
	code_s = 0; //the size of the match

	while(*p == ' ' || *p == '\t') p++;	//discard whitespace
	for(code=0; code < sizeof(inst_table)/sizeof(instruction_t) ;code++){ //look up the value of the code
		if(strncasecmp(p, inst_table[code].name, inst_table[code].size) == 0){
			if(inst_table[code].size > code_s){
				code_m = code;
				code_s = inst_table[code].size;
			}
		}
	}

	return code_m;
}

void get_line_addr(int * target){
	FILE * begin = fopen("hue.asm","rb");
	char linebuf[1024];
	int line=0;
	uint16_t addr=0;

	for(;fgets(linebuf,sizeof(linebuf), begin) && line < *target-1; line++){
		if(check_label((char *) &linebuf[0], line)) continue;
		addr++;
		int op = decode_op((char *) &linebuf[0]);
		addr += inst_table[op].args;
	}



	*target=(int) addr;
}

char * get_arg_str(arg_type at){
	if(at == NOARG) return "NOARG";
	if(at == VAL) return "VAL";
	if(at == REG) return "REG";
	return "Unknown arg type";
}

void as_exit(int e){
	if(ftell(fp) != -1L)
		fclose(out);

	if(ftell(out) != -1L)
		fclose(out);

	exit(e);
}

void set_arg_type(char *p, arg_type *at){
	int i;
	for(i=0;i<label_table_sz;i++){
		if(strncmp(p,label_table[i].name, strlen(label_table[i].name)) == 0){
			*at = VAL;
			return;
		}
	}

	if(strncasecmp(p, "ld", 2) == 0){
		*at = LINEDIFF;
	}else if(strncasecmp(p, "r", 1) == 0){
		*at = REG;
	}else if(strncasecmp(p, "l", 1) == 0){
		*at = LINE;
	}else{
		*at = VAL;
	}
}


void parse_arg(char *p, int l,uint16_t * val, arg_type * at){
	int a=0;

	while(*p == ' ' || *p == '\t') p++;	//discard whitespace
	set_arg_type(p, at);
	if(*at == VAL){
		for(a=0;a<label_table_sz;a++){
			if(strncmp(p,label_table[a].name, strlen(label_table[a].name)) == 0){
				*val = (uint16_t) label_table[a].addr;
				return;
			}
		}
	}
	if(*at == LINEDIFF) p+=2; //skip over the ld
	if(*at == LINE) p++; //skip over the l
	if(*at == REG)  p++; //skip over the r
	sscanf(p,"%i",&a);
	if(*at == LINEDIFF){
		a+=l+1;
		*at=LINE;
		p++;
	}
	if(*at == LINE){
		get_line_addr(&a);
		*at = VAL;
	}

	*val =(uint16_t) a;
}

void arg_errors(opinst_t op, uint8_t arg, int line){
	line++;

	if(inst_table[op.op].arg_t[arg] != op.arg_t[arg]){
	   WARNING("LINE: %i ARG%i is of type %s, should be %s\n",line,arg ,get_arg_str(op.arg_t[arg]),get_arg_str(inst_table[op.op].arg_t[arg]));
	}

	if(op.arg_t[arg] == REG &&
	   op.arg[arg] >= OP32_REG_NUM){
	   ERROR("LINE: %i ARG%i Register %i is out of bounds\n",line,arg ,op.arg[arg]);
	}

	if(op.arg[arg] >= OP32_MEM_SZ){
	   ERROR("LINE: %i ARG%i Memory overflow\n",line, arg);
	}

}



int main( int argc, char *argv[] ){
	char **in_name = NULL;
	char **out_name = NULL;

	out_name = (char **) &("a.bin");

	int i;
	for(i=1;i<argc;i++){
		if(strncmp(argv[i], "-o", 2) == 0){
			out_name = &argv[++i];
		}else{
			in_name = &argv[i];
		}
	}

	if(in_name == NULL){
		printf("OP32 Assembler\n"
			   "Usage: op32-as [args] [in]\n"
			   "-o [name] -- output file name\n");
		exit(1);
	}

	fp = fopen(*in_name,"rb");
	out = fopen(*out_name,"wb");

	char linebuf[1024];
	char *p;
	int line;

	for(line=0;fgets(linebuf,sizeof(linebuf), fp);line++){
		opinst_t curr_inst;
		p = &linebuf[0];

		while(*p == ' ' || *p == '\t') p++;	//discard whitespace
		if(*p == '\n' || *p == '\r' || *p == ';') continue; //check for empty line or comment
		if(check_label(p,line)) continue;

		curr_inst = (const opinst_t) {0}; //zero all fields in curr_inst

		curr_inst.op = decode_op(p);
		p += inst_table[curr_inst.op].size; //jump over current instruction
		if(inst_table[curr_inst.op].args == 0) goto FINISH_LINE_PARSE;

		parse_arg(p, line, &curr_inst.arg[0], &curr_inst.arg_t[0]);
		if(inst_table[curr_inst.op].args == 1) goto FINISH_LINE_PARSE;

		while(*p != ',') p++;	//skip over the , delimiting the arguments
		p++;

		parse_arg(p, line, &curr_inst.arg[1], &curr_inst.arg_t[1]);

		FINISH_LINE_PARSE:

		if(inst_table[curr_inst.op].args >= 1) arg_errors(curr_inst, 0, line);
		if(inst_table[curr_inst.op].args >= 2) arg_errors(curr_inst, 1, line);

		fputc((uint8_t) ((curr_inst.op & 0xFF00) >> 8), out);
		fputc((uint8_t) (curr_inst.op & 0x00FF), out);
		if(inst_table[curr_inst.op].args == 0) continue;
		fputc((uint8_t) ((curr_inst.arg[1] & 0xFF00) >> 8), out);
		fputc((uint8_t) (curr_inst.arg[2] & 0x00FF), out);
		if(inst_table[curr_inst.op].args == 1) continue;
		fputc((uint8_t) ((curr_inst.arg[2] & 0xFF00) >> 8), out);
		fputc((uint8_t) (curr_inst.arg[2] & 0x00FF), out);

	}

	fclose(fp);
	fclose(out);

	return 0;
}
