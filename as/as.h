#ifndef _H_AS_H_
#define _H_AS_H_

typedef enum {NOARG, VAL, REG, LINE, LINEDIFF} arg_type;

typedef struct instruction{
	const char * name;
	uint8_t size;
	uint16_t op;
	uint8_t args;
	arg_type arg_t[2];
} instruction_t;

typedef struct opinst{
	uint16_t op;
	uint16_t arg[2];
	arg_type arg_t[2];
} opinst_t;

typedef struct label{
	char * name;
	uint16_t addr;
} label_t;

uint8_t check_label(char *p, uint16_t l);
uint16_t decode_op(char *p);
void get_line_addr(int * target);
char * get_arg_str(arg_type at);
void as_exit(int e);
void set_arg_type(char *p, arg_type *at);
void parse_arg(char *p, int l,uint16_t * val, arg_type * at);
void arg_errors(opinst_t op, uint8_t arg, int line);

#endif //_H_AS_H_
