# OP32-AS
Assembler for the OP32 emulator

Syntax:
```
[INST] [ARG0], [ARG1]
```

Anything after ```[ARG1]``` will be treated as a comment

Directives

 - Address
	 - ```STOR 0x17, r4``` - Base16
	 - ```STOR 23, r4``` - Base 10
	 - ```STOR 027, r4``` - Base 8
 - Register - r
	 - ```LDV r15, 0x66```
 - Line number - l
	 - ```LDV r0, l0``` -- This will jump
	 - ```JMP r0``` -- to the instruction on line 0
 - Line Diff - ld
	 - ```NOP```
	 - ```LDV r0, ld5``` --This will jump
	 - ```JMP r0``` -- to the instruction on line 6
 - Label
 	 - ```thisIsALabel:```
 	 - ```LDV r0, thisIsALabel```
 	 - ```JMP r0```
 - Comment - ;
 	 - ```;This is a comment```