#ifndef SRC_VIDEO_H_
#define SRC_VIDEO_H_

#include <SDL/SDL.h>
#define VID_BPP 8
#define VID_FPS 60

void video_start(SDL_Surface **screen);
void clear_screen(SDL_Surface *screen);
void draw_pixel(SDL_Surface *screen, int x, int y, uint8_t color);
void refresh_display(SDL_Surface *screen, uint16_t *display);
void video_end();

#endif /* SRC_VIDEO_H_ */
