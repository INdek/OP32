#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>
#include "op32.h"
#include "op32defs.h"
#include "opcodes.h"

#define t_assert(message, test) do {										\
									 if (!(test)){							\
									 	fail++;								\
									 	fprintf(stderr, "%s\n", message);	\
									 }else{									\
									 	pass++;								\
									 }										\
									 total++;								\
								} while (0)

static uint32_t pass,fail,total;
static OP32 * cpu;



void buildOP_2(uint16_t loc, uint16_t op, uint16_t a, uint16_t b){
	cpu->memory[loc] = op;
	cpu->memory[loc+1] = a;
	cpu->memory[loc+2] = b;
}

void buildOP_1(uint16_t loc, uint16_t op, uint16_t a){
	cpu->memory[loc] = op;
	cpu->memory[loc+1] = a;
}

void buildOP_0(uint16_t loc, uint16_t op){
	cpu->memory[loc] = op;
}

void init_test(){
	cpu = op32_init();
	t_assert("CPU is NULL", cpu != NULL);
	t_assert("CPU->stack is NULL", cpu->stack != NULL);
	t_assert("CPU->R is NULL", cpu->R != NULL);
	t_assert("CPU->memory is NULL", cpu->memory != NULL);
}

void end_test(){
	op32_free(cpu);
}

void op32_nop_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	buildOP_0(0,OP32_NOP);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_NOP_TEST: diff->PC != 1", diff->OP32_PC == 1);
	op32_free(old);
	op32_free(diff);
}

void op32_ld_test(){
	op32_reset(cpu);
	cpu->memory[0x10] = 5;
	OP32 * old = op32_duplicate(cpu);
	buildOP_2(0,OP32_LD,1,0x10);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_LD_TEST: diff->R[1] != 5", diff->R[1] == 5);
	op32_free(old);
	op32_free(diff);
}

void op32_ldr_test(){
	op32_reset(cpu);
	cpu->memory[0x10] = 5;
	cpu->R[6] = 0x10;
	OP32 * old = op32_duplicate(cpu);
	buildOP_2(0,OP32_LDR,1,6);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_LDR_TEST: diff->R[1] != 5", diff->R[1] == 5);
	op32_free(old);
	op32_free(diff);
}
void op32_ldv_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	buildOP_2(0,OP32_LDV,0,5);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_LDV_TEST: diff->R[0] != 5", diff->R[0] == 5);
	op32_free(old);
	op32_free(diff);
}

void op32_sto_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	cpu->R[5] = 0xFD;
	buildOP_2(0,OP32_STO,0x10,5);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_STO_TEST: mem[0x10] != 0xFD", diff->memory[0x10] == 0xFD);
	op32_free(old);
	op32_free(diff);
}

void op32_stor_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	cpu->R[7] = 0x10;
	cpu->R[5] = 0xFD;
	buildOP_2(0,OP32_STOR,7,5);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_STOR_TEST: mem[0x10] != 0xFD", diff->memory[0x10] == 0xFD);
	op32_free(old);
	op32_free(diff);
}

void op32_addv_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	cpu->R[5] = 0xC0;
	buildOP_2(0,OP32_ADDV,5,0x2);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_ADDV_TEST: R[5] != 0xC2", diff->R[5] == 0xC2);
	op32_free(old);
	op32_free(diff);
}

void op32_subv_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	cpu->R[5] = 0xC6;
	buildOP_2(0,OP32_SUBV,5,0x3);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_SUBV_TEST: R[5] != 0xC2", diff->R[5] == 0xC3);
	op32_free(old);
	op32_free(diff);
}

void op32_addr_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	cpu->R[6] = 0x02;
	cpu->R[5] = 0xC0;
	buildOP_2(0,OP32_ADDR,5,6);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_ADDR_TEST: R[5] != 0xC2", diff->R[5] == 0xC2);
	op32_free(old);
	op32_free(diff);
}

void op32_subr_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	cpu->R[6] = 0x04;
	cpu->R[5] = 0xC5;
	buildOP_2(0,OP32_SUBR,5,6);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_SUBR_TEST: R[5] != 0xC2", diff->R[5] == 0xC1);
	op32_free(old);
	op32_free(diff);
}

void op32_mulv_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	cpu->R[13] = 0x4;
	buildOP_2(0,OP32_MULV,13,2);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_MULV_TEST: R[13] != 0x8", diff->R[13] == 0x08);
	op32_free(old);
	op32_free(diff);
}

void op32_divv_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	cpu->R[13] = 0x8;
	buildOP_2(0,OP32_DIVV,13,2);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_DIVV_TEST: R[13] != 0x4", diff->R[13] == 0x04);
	op32_free(old);
	op32_free(diff);
}

void op32_mulr_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	cpu->R[11] = 0x4;
	cpu->R[13] = 0x4;
	buildOP_2(0,OP32_MULR,13,11);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_MULR_TEST: R[13] != 0x10", diff->R[13] == 0x10);
	op32_free(old);
	op32_free(diff);
}

void op32_divr_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	cpu->R[11] = 0x4;
	cpu->R[13] = 0x4;
	buildOP_2(0,OP32_DIVR,13,11);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_DIVR_TEST: R[13] != 0x1", diff->R[13] == 0x1);
	op32_free(old);
	op32_free(diff);
}

void op32_push_test(){
	op32_reset(cpu);
	cpu->R[0] = 152;
	buildOP_1(0,OP32_PUSH, 0);
	op32_cycle(cpu);
	t_assert("OP32_PUSH_TEST: STACK != R[0]", cpu->stack[cpu->OP32_SP-1] == cpu->R[0]);
}

void op32_pop_test(){
	op32_reset(cpu);
	cpu->stack[cpu->OP32_SP++] = 99;
	buildOP_1(0,OP32_POP, 0);
	op32_cycle(cpu);
	t_assert("OP32_POP_TEST: R[0] != STACK", cpu->R[0] == 99);
}

void op32_jmp_test(){
	op32_reset(cpu);
	cpu->R[0] = 0xFFC0;
	buildOP_1(0,OP32_JMP,0);
	op32_cycle(cpu);
	t_assert("OP32_JMP_TEST: PC is wrong", cpu->OP32_PC == 0xFFC0);
}

void op32_jmpzr_test(){
	op32_reset(cpu);
	cpu->R[0] = 0;
	cpu->R[1] = 0x00FF;
	buildOP_2(0,OP32_JMPZR,0,1);
	op32_cycle(cpu);
	t_assert("OP32_JMPZR_TEST: JMP: true PC is wrong ", cpu->OP32_PC == 0x00FF);

	op32_reset(cpu);
	cpu->R[0] = 1;
	cpu->R[1] = 0x00FF;
	buildOP_2(0,OP32_JMPZR,0,1);
	op32_cycle(cpu);
	t_assert("OP32_JMPZR_TEST: JMP: false PC is wrong ", cpu->OP32_PC != 0x00FF);
}

void op32_jmpnzr_test(){
	op32_reset(cpu);
	cpu->R[0] = 0;
	cpu->R[1] = 0x00FF;
	buildOP_2(0,OP32_JMPNZR,0,1);
	op32_cycle(cpu);
	t_assert("OP32_JMPNZR_TEST: JMP: false PC is wrong ", cpu->OP32_PC != 0x00FF);

	op32_reset(cpu);
	cpu->R[0] = 1;
	cpu->R[1] = 0x00FF;
	buildOP_2(0,OP32_JMPNZR,0,1);
	op32_cycle(cpu);
	t_assert("OP32_JMPNZR_TEST: JMP: true PC is wrong ", cpu->OP32_PC == 0x00FF);
}

void op32_jmpzv_test(){
	op32_reset(cpu);
	cpu->R[0] = 0;
	buildOP_2(0,OP32_JMPZV,0,0x00FF);
	op32_cycle(cpu);
	t_assert("OP32_JMPZV_TEST: JMP: true PC is wrong ", cpu->OP32_PC == 0x00FF);

	op32_reset(cpu);
	cpu->R[0] = 1;
	buildOP_2(0,OP32_JMPZV,0,0x00FF);
	op32_cycle(cpu);
	t_assert("OP32_JMPZV_TEST: JMP: false PC is wrong ", cpu->OP32_PC != 0x00FF);
}

void op32_jmpnzv_test(){
	op32_reset(cpu);
	cpu->R[0] = 0;
	buildOP_2(0,OP32_JMPNZV,0,0x00FF);
	op32_cycle(cpu);
	t_assert("OP32_JMPNZV_TEST: JMP: false PC is wrong ", cpu->OP32_PC != 0x00FF);

	op32_reset(cpu);
	cpu->R[0] = 1;
	buildOP_2(0,OP32_JMPNZV,0,0x00FF);
	op32_cycle(cpu);
	t_assert("OP32_JMPNZV_TEST: JMP: true PC is wrong ", cpu->OP32_PC == 0x00FF);
}

void op32_and_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	cpu->R[11] = 0x4;
	cpu->R[13] = 0x4;
	buildOP_2(0,OP32_AND,13,11);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_AND_TEST: R[13] != 0x4", diff->R[13] == (0x4 & 0x4));
	op32_free(old);
	op32_free(diff);
}

void op32_or_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	cpu->R[11] = 0x4;
	cpu->R[13] = 0x1;
	buildOP_2(0,OP32_OR,13,11);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_OR_TEST: R[13] != 0x5", diff->R[13] == (0x4 | 0x1));
	op32_free(old);
	op32_free(diff);
}

void op32_xor_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	cpu->R[11] = 0x4;
	cpu->R[13] = 0x3;
	buildOP_2(0,OP32_XOR,13,11);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_XOR_TEST: R[13] != 0x7", diff->R[13] == (0x4 ^ 0x3));
	op32_free(old);
	op32_free(diff);
}

void op32_nand_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	cpu->R[11] = 0x4;
	cpu->R[13] = 0x4;
	buildOP_2(0,OP32_NAND,13,11);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_NAND_TEST: R[13] != 0xFFFB", diff->R[13] == (uint16_t) ~(0x4 & 0x4));
	op32_free(old);
	op32_free(diff);
}

void op32_nor_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	cpu->R[11] = 0x20;
	cpu->R[13] = 0x4;
	buildOP_2(0,OP32_NOR,13,11);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_NOR_TEST: R[13] != 0xFFDB", diff->R[13] == (uint16_t) ~(0x20 | 0x4));
	op32_free(old);
	op32_free(diff);
}

void op32_not_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	cpu->R[13] = 0x4;
	buildOP_1(0,OP32_NOT,13);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_NOT_TEST: R[13] != 0xFFFB", diff->R[13] == (uint16_t) ~(0x4));
	op32_free(old);
	op32_free(diff);
}

void op32_lsh_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	cpu->R[13] = 0x4;
	cpu->R[2] = 1;
	buildOP_2(0,OP32_LSH,13,2);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_LSH_TEST: R[13] != 0x8", diff->R[13] == (0x4 << 1));
	op32_free(old);
	op32_free(diff);
}

void op32_rsh_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	cpu->R[13] = 0x4;
	cpu->R[2] = 1;
	buildOP_2(0,OP32_RSH,13,2);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_RSH_TEST: R[13] != 0x2", diff->R[13] == (0x4 >> 1));
	op32_free(old);
	op32_free(diff);
}

void op32_cpy_test(){
	op32_reset(cpu);
	OP32 * old = op32_duplicate(cpu);
	cpu->R[13] = 0x4;
	buildOP_2(0,OP32_CPY,13,2);
	op32_cycle(cpu);
	OP32 * diff = op32_diff(cpu, old);
	t_assert("OP32_CPY_TEST: R[2] != R[13]", diff->R[13] == diff->R[2]);
	op32_free(old);
	op32_free(diff);
}

void op32_swp_test(){
	op32_reset(cpu);
	cpu->R[13] = 0x4;
	cpu->R[12] = 0x2;
	OP32 * old = op32_duplicate(cpu);
	buildOP_2(0,OP32_SWP,13,12);
	op32_cycle(cpu);
	t_assert("OP32_SWP_TEST: old->R[13] != cpu->R[12]", old->R[13] == cpu->R[12]);
	t_assert("OP32_SWP_TEST: old->R[12] != cpu->R[13]", old->R[12] == cpu->R[13]);
	op32_free(old);
}

void op32_mod_test(){
	op32_reset(cpu);
	cpu->R[13] = 0x4;
	cpu->R[12] = 0x2;
	OP32 * old = op32_duplicate(cpu);
	buildOP_2(0,OP32_MOD,13,12);
	op32_cycle(cpu);
	t_assert("OP32_MOD_TEST: R[13]%2 != 0", cpu->R[13] == old->R[13]%old->R[12]);
	op32_free(old);
}

void op32_stack_stress_test(){
	op32_reset(cpu);
	uint16_t i,miss=0;

	for(i=0;i<OP32_STACK_SZ;i++){
		cpu->R[0] = i;
		buildOP_1(0,OP32_PUSH,0);
		op32_cycle(cpu);
		cpu->OP32_PC = 0;
	}

	for(i=OP32_STACK_SZ;i-- > 0;){
		buildOP_1(0,OP32_POP,1);
		op32_cycle(cpu);
		if(cpu->R[1] != i)
			miss++;
		cpu->OP32_PC = 0;
	}

	t_assert("OP32_STACK_STRESS_TEST: Missed at least one value", miss==0);
}

void op32_mem_end_test(){
	op32_reset(cpu);
	uint32_t i;
	for(i=0;i<OP32_MEM_SZ;i++){
		op32_cycle(cpu);
	}
	t_assert("OP32_MEM_END_TEST: PC is not 0xFFFF", cpu->OP32_PC == 0xFFFF);
	op32_cycle(cpu);
	t_assert("OP32_MEM_END_TEST: PC is not 0x1", cpu->OP32_PC == 0x1);

}

void op32_file_load_test(){
	op32_reset(cpu);
	/*Write File*/
	uint16_t i;
	uint16_t error=0;
	uint16_t file[] = {
			OP32_LDV, 0, 10,
			OP32_MULV, 0, 2,
			OP32_PUSH, 0
	};
	FILE * fp = fopen("tmp_cmp_file.bin","wb");
	t_assert("OP32_FILE_LOAD_TEST: Unable to open file for writing", fp != NULL);
	for(i=0;i<8;i++){
		fputc(((file[i] & 0xFF00) >> 8),fp);
		fputc((file[i] & 0x00FF),fp);
	}
	fclose(fp);
	/*Write File*/

	fp = fopen("tmp_cmp_file.bin","rb");
	op32_load(cpu,fp);

	for(i=0;i<8;i++){
		if(cpu->memory[i] != file[i]){
			error++;
		}
	}
	t_assert("OP32_FILE_LOAD_TEST: Memory is not equal to file", !error);
	t_assert("OP32_FILE_LOAD_TEST: Unable to delete file", !remove("tmp_cmp_file.bin"));
}



int main( int argc, char *argv[] ){
	argc = argc; //remove warning
	argv = argv;

	init_test();

	op32_nop_test();
	op32_ld_test();
	op32_ldr_test();
	op32_ldv_test();
	op32_sto_test();
	op32_stor_test();
	op32_addv_test();
	op32_subv_test();
	op32_addr_test();
	op32_subr_test();
	op32_mulv_test();
	op32_divv_test();
	op32_mulr_test();
	op32_divr_test();
	op32_push_test();
	op32_pop_test();
	op32_jmp_test();
	op32_jmpzr_test();
	op32_jmpnzr_test();
	op32_jmpzv_test();
	op32_jmpnzv_test();
	op32_and_test();
	op32_or_test();
	op32_xor_test();
	op32_nand_test();
	op32_nor_test();
	op32_not_test();
	op32_lsh_test();
	op32_rsh_test();
	op32_cpy_test();
	op32_swp_test();
	op32_mod_test();
	op32_stack_stress_test();
	op32_mem_end_test();
	op32_file_load_test();

	fprintf(stdout,"REPORT:\n"
				   "Tests passed: %"PRIu32"\n"
				   "Tests failed: %"PRIu32"\n"
				   "Total tests:  %"PRIu32"\n",
				   pass,fail,total);

	end_test();
	return 0;
}
