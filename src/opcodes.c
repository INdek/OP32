#include <stdint.h>
#include "op32.h"
#include "opcodes.h"

void op32_nop(){
}

void op32_ld(OP32 * c, uint16_t val1, uint16_t val2){
	c->R[val1] = c->memory[val2];
}

void op32_ldr(OP32 * c, uint16_t val1, uint16_t val2){
	c->R[val1] = c->memory[c->R[val2]];
}

void op32_ldv(OP32 * c, uint16_t val1, uint16_t val2){
	c->R[val1] = val2;
}

void op32_sto(OP32 * c, uint16_t val1, uint16_t val2){
	c->memory[val1] = c->R[val2];
}

void op32_stor(OP32 * c, uint16_t val1, uint16_t val2){
	c->memory[c->R[val1]] = c->R[val2];
}

void op32_addv(OP32 * c, uint16_t val1, uint16_t val2){
	c->R[val1] += val2;
}

void op32_subv(OP32 * c, uint16_t val1, uint16_t val2){
	c->R[val1] -= val2;
}

void op32_addr(OP32 * c, uint16_t val1, uint16_t val2){
	c->R[val1] += c->R[val2];
}

void op32_subr(OP32 * c, uint16_t val1, uint16_t val2){
	c->R[val1] -= c->R[val2];
}

void op32_mulv(OP32 * c, uint16_t val1, uint16_t val2){
	c->R[val1] *= val2;
}

void op32_divv(OP32 * c, uint16_t val1, uint16_t val2){
	c->R[val1] /= val2;
}

void op32_mulr(OP32 * c, uint16_t val1, uint16_t val2){
	c->R[val1] *= c->R[val2];
}

void op32_divr(OP32 * c, uint16_t val1, uint16_t val2){
	c->R[val1] /= c->R[val2];
}

void op32_push(OP32 * c, uint16_t val){
	c->stack[c->OP32_SP] = c->R[val];
	c->OP32_SP++;
}

void op32_pop(OP32 * c, uint16_t val){
	c->OP32_SP--;
	c->R[val] = c->stack[c->OP32_SP];
	c->stack[c->OP32_SP] = 0;
}

void op32_jmp(OP32 * c, uint16_t val){
	c->OP32_PC = c->R[val];
}

void op32_jmpzr(OP32 * c, uint16_t val1, uint16_t val2){
	if(c->R[val1] == 0)
		c->OP32_PC = c->R[val2];
}

void op32_jmpnzr(OP32 * c, uint16_t val1, uint16_t val2){
	if(c->R[val1] != 0)
		c->OP32_PC = c->R[val2];
}

void op32_jmpzv(OP32 * c, uint16_t val1, uint16_t val2){
	if(c->R[val1] == 0)
		c->OP32_PC = val2;
}

void op32_jmpnzv(OP32 * c, uint16_t val1, uint16_t val2){
	if(c->R[val1] != 0)
		c->OP32_PC = val2;
}

void op32_and(OP32 * c, uint16_t val1, uint16_t val2){
	c->R[val1] &= c->R[val2];
}

void op32_or(OP32 * c, uint16_t val1, uint16_t val2){
	c->R[val1] |= c->R[val2];
}

void op32_xor(OP32 * c, uint16_t val1, uint16_t val2){
	c->R[val1] ^= c->R[val2];
}

void op32_nand(OP32 * c, uint16_t val1, uint16_t val2){
	c->R[val1] = ~(c->R[val1] & c->R[val2]);
}

void op32_nor(OP32 * c, uint16_t val1, uint16_t val2){
	c->R[val1] = ~(c->R[val1] | c->R[val2]);
}
void op32_not(OP32 * c, uint16_t val){
	c->R[val] = ~c->R[val];
}

void op32_lsh(OP32 * c, uint16_t val1, uint16_t val2){
	c->R[val1] <<= c->R[val2];
}

void op32_rsh(OP32 * c, uint16_t val1, uint16_t val2){
	c->R[val1] >>= c->R[val2];
}

void op32_cpy(OP32 * c, uint16_t val1, uint16_t val2){
	c->R[val1] = c->R[val2];
}
void op32_swp(OP32 * c, uint16_t val1, uint16_t val2){
	uint16_t x = c->R[val1];
	c->R[val1] = c->R[val2];
	c->R[val2] = x;
}

void op32_mod(OP32 * c, uint16_t val1, uint16_t val2){
	c->R[val1] %= c->R[val2];
}
