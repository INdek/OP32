#ifdef _WIN32
#include <windows.h>
#endif

#include <SDL/SDL.h>
#include <inttypes.h>
#include <stdio.h>
#include "video.h"
#include "op32.h"

void video_start(SDL_Surface **screen){
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "ERROR: Unable to init SDL: %s\n", SDL_GetError());
		exit(1);
	}

	*screen = SDL_SetVideoMode(OP32_VID_X * VID_BPP, OP32_VID_Y * VID_BPP, 8, SDL_SWSURFACE);
	if (*screen == NULL) {
		fprintf(stdout, "ERROR: Unable to set video: %s\n", SDL_GetError());
		exit(2);
	}
}

void clear_screen(SDL_Surface *screen){
	SDL_FillRect(screen, NULL, 0);
}

void draw_pixel(SDL_Surface *screen, int x, int y, uint8_t color){

	uint8_t *pixel;
	int i, j;

	pixel = (uint8_t *)screen->pixels + ((y * VID_BPP) * screen->pitch) + (x * VID_BPP);
	for (i = 0; i < VID_BPP; i++) {
		for (j = 0; j < VID_BPP; j++) {
			pixel[j] = color;
		}
		pixel += screen->pitch;
	}

}

void refresh_display(SDL_Surface *screen, uint16_t *display){
	int x, y;

	if (SDL_MUSTLOCK(screen)) {
		SDL_LockSurface(screen);
	}

	clear_screen(screen);
	for (y = 0; y < OP32_VID_Y; y++) {
		for (x = 0; x < OP32_VID_X; x++) {
			uint8_t p = (uint8_t) display[x + (y * OP32_VID_X)];
				draw_pixel(screen, x, y, p);
		}
	}

	if (SDL_MUSTLOCK(screen)) {
		SDL_UnlockSurface(screen);
	}

	SDL_Flip(screen);
}

void video_end(){
	SDL_Quit();
}
