#ifdef _WIN32
#include <windows.h>
#endif
#ifndef _WIN32
#include <unistd.h>
#endif


#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "op32.h"
#include "opcodes.h"
#include "video.h"



typedef union {
	void (*zero)();
	void (*one)(OP32 * c, uint16_t val);
	void (*two)(OP32 * c, uint16_t val1, uint16_t val2);
} op32_op;

static op32_op opfunc[32] = {
								{.zero = op32_nop},
								{.two = op32_ld},
								{.two = op32_ldr},
								{.two = op32_ldv},
								{.two = op32_sto},
								{.two = op32_stor},
								{.two = op32_addv},
								{.two = op32_subv},
								{.two = op32_addr},
								{.two = op32_subr},
								{.two = op32_mulv},
								{.two = op32_divv},
								{.two = op32_mulr},
								{.two = op32_divr},
								{.one = op32_push},
								{.one = op32_pop},
								{.one = op32_jmp},
								{.two = op32_jmpzr},
								{.two = op32_jmpnzr},
								{.two = op32_jmpzv},
								{.two = op32_jmpnzv},
								{.two = op32_and},
								{.two = op32_or},
								{.two = op32_xor},
								{.two = op32_nand},
								{.two = op32_nor},
								{.one = op32_not},
								{.two = op32_lsh},
								{.two = op32_rsh},
								{.two = op32_cpy},
								{.two = op32_swp},
								{.two = op32_mod}
				     		};

OP32 * op32_init(void){

	OP32 * c = malloc(sizeof(OP32));

	if(c == NULL){
		fprintf(stderr, "MEMERROR: Unable to create CPU\n");
		exit(1);
	}

	c->memory = malloc( OP32_MEM_SZ  * sizeof(uint16_t) );
	c->R = malloc( OP32_REG_NUM * sizeof(uint16_t) );
	c->stack = malloc( OP32_STACK_SZ * sizeof(uint16_t));
	c->screen = NULL;

	memset(c->memory, 0, OP32_MEM_SZ* sizeof(uint16_t));
	memset(c->R, 0, OP32_REG_NUM * sizeof(uint16_t));
	memset(c->stack, 0, OP32_STACK_SZ * sizeof(uint16_t));

	return c;
}

void op32_reset(OP32 * c){
	memset(c->memory, 0, OP32_MEM_SZ * sizeof(uint16_t));
	memset(c->R, 0, OP32_REG_NUM * sizeof(uint16_t));
	memset(c->stack, 0, OP32_STACK_SZ * sizeof(uint16_t));
}

void op32_load(OP32 * c, FILE * fp){
	size_t sz,i;

	if(fp == NULL){
		fprintf(stderr,"READERROR: Unable to load ROM\nCONTINUING\n");
		exit(1);
	}

	fseek(fp, 0L, SEEK_END);
	sz = ftell(fp);
	fseek(fp, 0L, SEEK_SET);

	if(sz/2 > OP32_MEM_SZ){
		fprintf(stderr,"MEMOVERFLOW: ROM larger than memory: %i\n",sz);
		exit(1);
	}

	for(i=0;i<sz;i++){
		uint8_t a,b;
		a = fgetc(fp);
		b = fgetc(fp);
		c->memory[i] = (a << 8) | b;
	}

	fclose(fp);

	return;
}

void op32_cycle(OP32 * c){
	c->OP32_PC %= OP32_MEM_SZ;

	uint16_t opcode = c->memory[c->OP32_PC];
	uint16_t arg1 = c->memory[c->OP32_PC+1];
	uint16_t arg2 = c->memory[c->OP32_PC+2];

	if(opcode == OP32_NOP){
		c->OP32_PC++;
	}else if((opcode >= OP32_PUSH && opcode <= OP32_JMP) || opcode == OP32_NOT){
		c->OP32_PC += 2;
		opfunc[opcode].one(c,arg1);
	}else{
		c->OP32_PC += 3;
		opfunc[opcode].two(c,arg1,arg2);
	}
}

OP32 * op32_duplicate(OP32 * a){
	OP32 * r = op32_init();
	uint16_t i;

	for(i=0;i<OP32_MEM_SZ;i++){
		r->memory[i] = a->memory[i];
	}

	for(i=0;i<OP32_REG_NUM;i++){
		r->R[i] = a->R[i];
	}

	for(i=0;i<OP32_STACK_SZ;i++){
		r->stack[i] = a->stack[i];
	}

	r->screen = NULL;
	return r;
}

OP32 * op32_diff(OP32 * a, OP32 * b){
	OP32 * diff = op32_init();
	uint16_t i;

	for(i=0;i<OP32_MEM_SZ;i++){
		diff->memory[i] = a->memory[i] ^ b->memory[i];
	}

	for(i=0;i<OP32_REG_NUM;i++){
		diff->R[i] = a->R[i] ^ b->R[i];
	}

	for(i=0;i<OP32_STACK_SZ;i++){
		diff->stack[i] = a->stack[i] ^ b->stack[i];
	}

	return diff;
}

void op32_refresh_display(OP32 * a){
	if(a->OP32_RS)
		refresh_display(a->screen, &(a->memory[0xFDFF]) );
}

void op32_free(OP32 * c){
	free(c->memory);
	free(c->stack);
	free(c->R);
	free(c);
}
