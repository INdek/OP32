#ifndef _H_OPCODES_H_
#define _H_OPCODES_H_

#include "op32.h"

#define OP32_NOP    0x00 //NOP
#define OP32_LD     0x01 //LOAD to REGSITER from ADDRESS
#define OP32_LDR    0x02 //LOAD to REGISTER from MEMORY[REGISTER]
#define OP32_LDV    0x03 //SET REGISTER to VALUE
#define OP32_STO    0x04 //STORE to ADDRESS from REGISTER
#define OP32_STOR   0x05 //STORE to MEMORY[REGISTER] from REGISTER
#define OP32_ADDV   0x06 //ADD on REGISTER VALUE
#define OP32_SUBV   0x07 //SUBTRACT on REGISTER VALUE
#define OP32_ADDR   0x08 //ADD on REGISTER REGSITER
#define OP32_SUBR   0x09 //SUBTRACT on REGISTER REGISTER
#define OP32_MULV   0x0A //MULTIPLY on REGISTER by VALUE
#define OP32_DIVV   0x0B //DIVIDE on REGISTER by VALUE
#define OP32_MULR   0x0C //MULTIPLY on REGISTER by REGISTER
#define OP32_DIVR   0x0D //DIVIDE on REGISTER by REGISTER
#define OP32_PUSH   0x0E //PUSH REGISTER to stack
#define OP32_POP    0x0F //POP to REGISTER from stack
#define OP32_JMP    0x10 //JUMP to REGISTER
#define OP32_JMPZR  0x11 //JUMP IF REGISTER==0 to REGISTER
#define OP32_JMPNZR 0x12 //JUMP IF REGISTER!=0 to REGISTER
#define OP32_JMPZV  0x13 //JUMP IF REGISTER==0 to VALUE
#define OP32_JMPNZV 0x14 //JUMP IF REGISTER!=0 to VALUE
#define OP32_AND    0x15 //AND REGISTER WITH REGISTER STORE ON FIRST
#define OP32_OR     0x16 //OR REGISTER WITH REGISTER STORE ON FIRST
#define OP32_XOR    0x17 //XOR REGISTER WITH REGISTER STORE ON FIRST
#define OP32_NAND   0x18 //NAND REGISTER WITH REGISTER STORE ON FIRST
#define OP32_NOR    0x19 //NOR REGISTER WITH REGSITER STORE ON FIRST
#define OP32_NOT    0x1A //NEGATE REGISTER
#define OP32_LSH    0x1B //SHIFT REGISTER LEFT BY REGISTER
#define OP32_RSH    0x1C //SHIFT REGISTER RIGHT BY REGISTER
#define OP32_CPY    0x1D //COPY TO REGISTER FROM REGISTER
#define OP32_SWP    0x1E //SWAP REGISTER WITH REGISTER
#define OP32_MOD    0x1F //MODULE REGISTER BY REGISTER (REG %= REG)

void op32_nop();
void op32_ld(OP32 * c, uint16_t val1, uint16_t val2);
void op32_ldr(OP32 * c, uint16_t val1, uint16_t val2);
void op32_ldv(OP32 * c, uint16_t val1, uint16_t val2);
void op32_sto(OP32 * c, uint16_t val1, uint16_t val2);
void op32_stor(OP32 * c, uint16_t val1, uint16_t val2);
void op32_addv(OP32 * c, uint16_t val1, uint16_t val2);
void op32_subv(OP32 * c, uint16_t val1, uint16_t val2);
void op32_addr(OP32 * c, uint16_t val1, uint16_t val2);
void op32_subr(OP32 * c, uint16_t val1, uint16_t val2);
void op32_mulv(OP32 * c, uint16_t val1, uint16_t val2);
void op32_divv(OP32 * c, uint16_t val1, uint16_t val2);
void op32_mulr(OP32 * c, uint16_t val1, uint16_t val2);
void op32_divr(OP32 * c, uint16_t val1, uint16_t val2);
void op32_push(OP32 * c, uint16_t val);
void op32_pop(OP32 * c, uint16_t val);
void op32_jmp(OP32 * c, uint16_t val);
void op32_jmpzr(OP32 * c, uint16_t val1, uint16_t val2);
void op32_jmpnzr(OP32 * c, uint16_t val1, uint16_t val2);
void op32_jmpzv(OP32 * c, uint16_t val1, uint16_t val2);
void op32_jmpnzv(OP32 * c, uint16_t val1, uint16_t val2);
void op32_and(OP32 * c, uint16_t val1, uint16_t val2);
void op32_or(OP32 * c, uint16_t val1, uint16_t val2);
void op32_xor(OP32 * c, uint16_t val1, uint16_t val2);
void op32_nand(OP32 * c, uint16_t val1, uint16_t val2);
void op32_nor(OP32 * c, uint16_t val1, uint16_t val2);
void op32_not(OP32 * c, uint16_t val);
void op32_lsh(OP32 * c, uint16_t val1, uint16_t val2);
void op32_rsh(OP32 * c, uint16_t val1, uint16_t val2);
void op32_cpy(OP32 * c, uint16_t val1, uint16_t val2);
void op32_swp(OP32 * c, uint16_t val1, uint16_t val2);
void op32_mod(OP32 * c, uint16_t val1, uint16_t val2);

#endif

