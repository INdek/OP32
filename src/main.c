#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "opcodes.h"
#include "op32.h"
#include "video.h"

void framelimit(uint32_t nextframe, int maxfps){
	uint32_t ticks = SDL_GetTicks();

	if (nextframe < ticks) {
		return;
	}

	if (nextframe > (ticks + maxfps)) {
		SDL_Delay(maxfps);
	} else {
		SDL_Delay(nextframe - ticks);
	}
}

int main( int argc, char *argv[] ){
	if(argc == 1){
		printf("OP32 Emulator\n"
			   "Usage: op32 [rom]\n");
		exit(1);
	}

	SDL_Event e;

	OP32 * c = op32_init();
	video_start(&c->screen);

	FILE * fp = fopen(argv[1],"rb");
	op32_load(c,fp);
	fclose(fp);


	while(1){
		 while( SDL_PollEvent( &e ) ) {
			 if( e.type == SDL_QUIT ) {
				 exit(0);
			 }
		 }

		op32_cycle(c);
		op32_refresh_display(c);
		//SDL_Delay(1000 / OP32_CLOCK);
	}

	return 1;
}



