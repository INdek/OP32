#ifndef SRC_OP32DEFS_H_
#define SRC_OP32DEFS_H_

#define OP32_MEM_SZ 0xFFFF
#define OP32_STACK_SZ 64
#define OP32_REG_NUM 19
#define OP32_VID_X 32
#define OP32_VID_Y 16
#define OP32_CLOCK 256UL //256Hz

#define OP32_SP R[16]
#define OP32_PC R[17]
#define OP32_RS R[18]

#endif /* SRC_OP32DEFS_H_ */
