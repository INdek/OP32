#ifndef _H_OP32_H_
#define _H_OP32_H_

#include <stdio.h>
#include <inttypes.h>
#include <SDL/SDL.h>
#include "op32defs.h"

typedef struct OP32{
	uint16_t * memory;
	uint16_t * R;
	uint16_t * stack;
	SDL_Surface *screen;
} OP32;

OP32 * op32_init(void);
void op32_load(OP32 * c, FILE * fp);
void op32_cycle(OP32 * c);
void op32_free(OP32 * c);
void op32_reset(OP32 * c);
OP32 * op32_duplicate(OP32 * a);
OP32 * op32_diff(OP32 * a, OP32 * b);
void op32_refresh_display(OP32 * a);

#endif
